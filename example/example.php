<?php
require __DIR__ . '/../vendor/autoload.php';
include 'config.php';

$hashedContent = new \MmoPaymentClient\HashedContent('identifier', 'Name Surname', 1050, true);
$hashedContent->addParam('sicilNo', '12344');
$hashedContent->addVisibleParam('Sicil No', '12344');
$hashedContent->addParam('tcKimlikNo', '123445345346');
$hashedContent->addVisibleParam('TC Kimlik No', '123445345346');
$hashedContent->addParam('trainigId', '100');
$hashedContent->addVisibleParam('Eğitim Adı', 'Kaynakçı Eğitimi');

$parameters = new \MmoPaymentClient\Parameters(P_UUID, $hashedContent);

$url = \MmoPaymentClient\BuildUrl::buildFromParameters(
    P_BASE_URL,
    P_SECRET_KEY,
    $parameters
);

echo '<script> window.location = "'.$url.'"; </script>';
