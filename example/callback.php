<?php
require __DIR__ . '/../vendor/autoload.php';
include 'config.php';

$content = $_GET['content'];
$contentData = \MmoPaymentClient\Hash::decrypt(P_SECRET_KEY, $content);
$parsedObject = json_decode($contentData, true);


if ($parsedObject['succeed']) {
    $oid = $parsedObject['transactionId']; // your identifier for the transaction
} else {
    echo $parsedObject['message'] ?? 'Unknown error';
}
