<?php
/**
 * Parameters
 *
 * @since     Jan 2019
 * @author    Haydar KULEKCI <haydarkulekci@gmail.com>
 */
namespace MmoPaymentClient;

class Parameters
{
    /** @var string */
    protected $uuid;
    /** @var HashedContent */
    protected $content;

    public function __construct($uuid, HashedContent $content)
    {
        $this->uuid = $uuid;
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return (string) $this->content;
    }
}
