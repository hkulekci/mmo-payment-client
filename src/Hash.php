<?php
/**
 * QueryParameter
 *
 * @since     Dec 2018
 * @author    Haydar KULEKCI <haydarkulekci@gmail.com>
 */

namespace MmoPaymentClient;


class Hash
{
    const CIPHER_METHOD = 'AES-256-CTR';
    const DIGEST_METHOD = 'SHA256';

    public static function encrypt($secret, $token)
    {
        $token = base64_encode($token);
        $encKey = \openssl_digest($secret, self::DIGEST_METHOD, true);
        $encIV = \openssl_random_pseudo_bytes(
            \openssl_cipher_iv_length(self::CIPHER_METHOD),
            $isSourceStrong
        );
        if (false === $isSourceStrong || false === $encIV) {
            throw new \RuntimeException('IV generation failed');
        }
        $cryptedToken = \openssl_encrypt(
            $token,
            self::CIPHER_METHOD,
            $encKey,
            0,
            $encIV
        ) . '::' . \bin2hex($encIV);
        unset($encKey, $encIV);

        return $cryptedToken;
    }

    public static function decrypt($secret, $token)
    {
        list($cryptedToken, $encIV) = explode('::', $token);
        $encKey = \openssl_digest(
            $secret,
            self::DIGEST_METHOD,
            true
        );
        $token = \openssl_decrypt(
            $cryptedToken,
            self::CIPHER_METHOD,
            $encKey,
            0,
            \hex2bin($encIV)
        );
        unset($cryptedToken, $encKey, $encIV);

        return base64_decode($token);
    }
}
