<?php
/**
 * BuildUrl
 *
 * @since     Jan 2019
 * @author    Haydar KULEKCI <haydarkulekci@gmail.com>
 */
namespace MmoPaymentClient;

class BuildUrl
{
    public static function buildFromParameters($baseUrl, $secretKey, Parameters $parameters)
    {
        $hashedContent = Hash::encrypt($secretKey, (string)$parameters->getContent());

        return $baseUrl. 'payment/start?uuid='.urlencode($parameters->getUuid()).
            '&content='.urlencode($hashedContent);
    }
}
