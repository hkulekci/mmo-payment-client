<?php
/**
 * HashedContent
 *
 * @since     Jan 2019
 * @author    Haydar KULEKCI <haydarkulekci@gmail.com>
 */
namespace MmoPaymentClient;

class HashedContent
{
    /**
     * @var string
     */
    protected $transactionId;

    /**
     * @var string
     */
    protected $nameSurname;

    /**
     * Should be integer like 1000 for 10,00TL
     *
     * @var integer
     */
    protected $amount;

    /**
     * Should be boolean to show installment or not
     *
     * @var boolean
     */
    protected $installment = true;

    /**
     * This contains the parameters which appear in the payment page.
     *
     * @var array
     */
    protected $params = array();

    /**
     * This contains the parameters which appear in the payment page.
     *
     * @var array
     */
    protected $visibleParams = array();

    public function __construct($transactionId, $nameSurname, $amount, $installment = true)
    {
        $this->transactionId = $transactionId;
        $this->nameSurname = $nameSurname;
        $this->amount = $amount;
        $this->installment = $installment;
    }

    public function addParam($key, $value)
    {
        $this->params[$key] = $value;
    }

    public function addVisibleParam($key, $value)
    {
        $this->visibleParams[$key] = $value;
    }

    public function __toString()
    {
        $data = json_encode($this->toArray());
        if (is_string($data) && $data) {
            return $data;
        }

        return '';
    }

    public function toArray()
    {
        return array(
            'transactionId' => $this->transactionId,
            'nameSurname' => $this->nameSurname,
            'amount' => $this->amount,
            'installment' => $this->installment,
            'params' => $this->params,
            'visibleParams' => $this->visibleParams,
        );
    }
}
