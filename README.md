## MMO Payment Client

Use composer to install or clone the repository in to your codebase.
This client helps you to create a redirect URL to reach the payment
page.

You need to provide an Base Url, UUID and Secret to use this library.

```
<?php
require __DIR__ . '/../vendor/autoload.php';

define(P_UUID, '123456');
define(P_SECRET_KEY, '123456');
define(P_BASE_URL, 'http://127.0.0.1:7070/');

$hashedContent = new \MmoPaymentClient\HashedContent('identifier', 'Name Surname', 1050);
$hashedContent->addParam('sicilNo', '12344');
$hashedContent->addVisibleParam('Sicil No', '12344');
$hashedContent->addParam('tcKimlikNo', '123445345346');
$hashedContent->addVisibleParam('TC Kimlik No', '123445345346');

$parameters = new \MmoPaymentClient\Parameters(P_UUID, $hashedContent);

$url = \MmoPaymentClient\BuildUrl::buildFromParameters(
    P_BASE_URL,
    P_SECRET_KEY,
    $parameters
);

echo $url . PHP_EOL;
```

The url will be like below:

```
https://base-url-for-payment-system.com/payment/start?uuid=your-uuid&content=hashed-content-with-secret
```

### Payment Flow

User: the person who using the payment system for paying.
Client: the person who want to use payment system on the own project.
Provider : the person who provider of the payment system.


 - The client will provide a redirect action in a pop-up for the payment page.
 - The provider will show payment methods and payment page.
 - The user will make a payment for the application
 - The provider will redirect the page to the client redirect_url.
 - The client will take the provider response from url.
